package utils

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type SuccessResponse struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
}

type ErrorResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

func SendSuccessResponse(c echo.Context, data interface{}) error {
	response := SuccessResponse{
		Success: true,
		Data:    data,
	}
	return c.JSON(http.StatusOK, response)
}

func SendErrorResponse(c echo.Context, message string) error {
	response := ErrorResponse{
		Success: false,
		Message: message,
	}
	return c.JSON(http.StatusBadRequest, response)
}
