package utils

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"os"
	"regexp"
	"sawit_pro_exam/domain/dto"
	"unicode"
)

func IsValidPhoneNumber(phoneNumber string) bool {
	validPattern := `^\+62\d{9,12}$`
	match, _ := regexp.MatchString(validPattern, phoneNumber)
	return match
}

func IsValidFullName(fullName string) bool {
	return len(fullName) >= 3 && len(fullName) <= 60
}

func IsValidPassword(password string) bool {
	if len(password) < 6 || len(password) > 64 {
		return false
	}

	hasCapital := false
	hasNumber := false
	hasSpecial := false

	for _, char := range password {
		switch {
		case unicode.IsUpper(char):
			hasCapital = true
		case unicode.IsNumber(char):
			hasNumber = true
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			hasSpecial = true
		}

		if hasCapital && hasNumber && hasSpecial {
			break
		}
	}

	return hasCapital && hasNumber && hasSpecial
}

func ValidateLoginRequest(request *dto.LoginRequest) error {
	if !IsValidPhoneNumber(request.PhoneNumber) {
		return echo.NewHTTPError(http.StatusBadRequest, "Invalid phone number")
	}

	if len(request.Password) < 6 || len(request.Password) > 64 {
		return echo.NewHTTPError(http.StatusBadRequest, "Invalid password")
	}

	return nil
}

func ComparePasswords(hashedPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}

func GenerateJWTToken(userID int64) (string, error) {
	claims := &dto.JwtClaimDto{
		UserID: userID,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	jwtSecret := []byte(os.Getenv("JWT_SECRET_KEY"))
	tokenString, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ExtractTokenFromHeader(header string) string {
	const bearerPrefix = "Bearer "
	if len(header) > len(bearerPrefix) && header[:len(bearerPrefix)] == bearerPrefix {
		return header[len(bearerPrefix):]
	}
	return ""
}