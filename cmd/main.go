package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"os"
	"sawit_pro_exam/handler"
)

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	apiPrefix := "/api/v1"
	apiGroup := e.Group(apiPrefix)

	apiGroup.POST("/register", handler.RegisterUser)
	apiGroup.POST("/login", handler.LoginUser)
	apiGroup.GET("/profile", handler.GetProfile, middleware.JWT([]byte(os.Getenv("JWT_SECRET_KEY"))))
	apiGroup.PUT("/update-profile", handler.UpdateProfile, middleware.JWT([]byte(os.Getenv("JWT_SECRET_KEY"))))

	log.Fatal(e.Start(":8000"))
}
