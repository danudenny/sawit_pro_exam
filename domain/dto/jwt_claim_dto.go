package dto

import "github.com/dgrijalva/jwt-go"

type JwtClaimDto struct {
	UserID 				int64 `json:"user_id"`
	jwt.StandardClaims
}
