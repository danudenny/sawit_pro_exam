package dto

type GetProfileResponse struct {
	ID 			uint64 `json:"id"`
	FullName    string `json:"full_name"`
	PhoneNumber string `json:"phone_number"`
}