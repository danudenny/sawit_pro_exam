package dto

type UserDto struct {
	ID 			uint64 `json:"id"`
	PhoneNumber string `json:"phone_number"`
	FullName 	string `json:"full_name"`
	Password 	string `json:"password"`
}
