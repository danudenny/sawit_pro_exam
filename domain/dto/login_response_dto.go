package dto

type LoginResponse struct {
	Token 	string 		`json:"token"`
	User    UserDto 	`json:"user"`
}