package dto

type UpdateProfileDto struct {
	PhoneNumber string `json:"phone_number"`
	FullName    string `json:"full_name"`
}
