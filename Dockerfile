# Build stage
FROM golang:1.19 AS builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o /app/main ./cmd

# Final stage
FROM golang:1.19
WORKDIR /app
COPY .env /app
COPY --from=builder /app/main .
EXPOSE 8000
CMD ["./main"]
