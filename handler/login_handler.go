package handler

import (
	"github.com/labstack/echo/v4"
	"sawit_pro_exam/domain/dto"
	"sawit_pro_exam/repositories"
	"sawit_pro_exam/utils"
)

func LoginUser(c echo.Context) error {
	request := new(dto.LoginRequest)
	if err := c.Bind(request); err != nil {
		return utils.SendErrorResponse(c, err.Error())
	}

	if err := utils.ValidateLoginRequest(request); err != nil {
		return utils.SendErrorResponse(c, err.Error())
	}

	user, err := repositories.GetUserByPhoneNumber(request.PhoneNumber)
	if err != nil {
		return utils.SendErrorResponse(c, "Invalid credentials")
	}

	if !utils.ComparePasswords(user.Password, request.Password) {
		return utils.SendErrorResponse(c, "Invalid credentials")
	}

	token, err := utils.GenerateJWTToken(int64(user.ID))
	if err != nil {
		return utils.SendErrorResponse(c, "Failed to generate token")
	}

	response := dto.LoginResponse{
		Token: token,
		User:  *user,
	}

	return utils.SendSuccessResponse(c, response)
}