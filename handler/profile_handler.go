package handler

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"log"
	"os"
	"sawit_pro_exam/domain/dto"
	"sawit_pro_exam/repositories"
	"sawit_pro_exam/utils"
)

func GetProfile(c echo.Context) error {
	authHeader := c.Request().Header.Get("Authorization")
	if authHeader == "" {
		return utils.SendErrorResponse(c, "Missing authorization header")
	}

	token := utils.ExtractTokenFromHeader(authHeader)
	if token == "" {
		return utils.SendErrorResponse(c, "Invalid authorization header format")
	}

	claims := &dto.JwtClaimDto{}
	jwtSecret := []byte(os.Getenv("JWT_SECRET_KEY"))
	parsedToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return utils.SendErrorResponse(c, "Invalid token signature")
		}
		return utils.SendErrorResponse(c, "Invalid token")
	}

	if !parsedToken.Valid {
		return utils.SendErrorResponse(c, "Token is invalid")
	}

	userID := claims.UserID
	user, err := repositories.GetUserByID(userID)
	log.Println(user)
	if err != nil {
		return utils.SendErrorResponse(c, "User not found")
	}

	response := dto.GetProfileResponse{
		ID:          user.ID,
		FullName:    user.FullName,
		PhoneNumber: user.PhoneNumber,
	}

	return utils.SendSuccessResponse(c, response)
}
