package handler

import (
	"sawit_pro_exam/models"
	"sawit_pro_exam/repositories"
	"sawit_pro_exam/utils"

	"github.com/labstack/echo/v4"
)

func RegisterUser(c echo.Context) error {
	user := new(models.User)
	if err := c.Bind(user); err != nil {
		return utils.SendErrorResponse(c, err.Error())
	}

	if !utils.IsValidPhoneNumber(user.PhoneNumber) {
		return utils.SendErrorResponse(c, "Invalid phone number")
	}

	if !utils.IsValidFullName(user.FullName) {
		return utils.SendErrorResponse(c, "Invalid full name")
	}

	if !utils.IsValidPassword(user.Password) {
		return utils.SendErrorResponse(c, "Invalid password")
	}

	err := repositories.CreateUser(user)
	if err != nil {
		return utils.SendErrorResponse(c, err.Error())
	}

	return utils.SendSuccessResponse(c, user)
}
