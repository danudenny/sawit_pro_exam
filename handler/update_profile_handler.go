package handler

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"os"
	"sawit_pro_exam/domain/dto"
	"sawit_pro_exam/repositories"
	"sawit_pro_exam/utils"
)

func UpdateProfile(c echo.Context) error {
	authHeader := c.Request().Header.Get("Authorization")
	if authHeader == "" {
		return utils.SendErrorResponse(c, "Missing authorization header")
	}

	token := utils.ExtractTokenFromHeader(authHeader)
	if token == "" {
		return utils.SendErrorResponse(c, "Invalid authorization header format")
	}

	claims := &dto.JwtClaimDto{}
	jwtSecret := []byte(os.Getenv("JWT_SECRET_KEY"))
	parsedToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil || !parsedToken.Valid {
		return utils.SendErrorResponse(c, "Invalid token")
	}

	userID := claims.UserID

	user, err := repositories.GetUserByID(userID)
	if err != nil {
		return utils.SendErrorResponse(c, "Failed to get user")
	}

	updateRequest := &dto.UpdateProfileDto{}
	err = c.Bind(&updateRequest)
	if err != nil {
		return utils.SendErrorResponse(c, "Failed to parse request body")
	}

	log.Println(updateRequest.PhoneNumber)

	if !utils.IsValidPhoneNumber(updateRequest.PhoneNumber) {
		return utils.SendErrorResponse(c, "Invalid phone number")
	}

	if !utils.IsValidFullName(updateRequest.FullName) {
		return utils.SendErrorResponse(c, "Invalid full name")
	}

	if updateRequest.PhoneNumber != "" && updateRequest.PhoneNumber != user.PhoneNumber {
		existingUser, _ := repositories.GetUserByPhoneNumber(updateRequest.PhoneNumber)
		if existingUser != nil {
			return c.JSON(http.StatusConflict, map[string]string{"error": "Phone number already exists"})
		}
	}

	if updateRequest.PhoneNumber != "" {
		user.PhoneNumber = updateRequest.PhoneNumber
	}
	if updateRequest.FullName != "" {
		user.FullName = updateRequest.FullName
	}

	err = repositories.UpdateUser(user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to update user"})
	}

	return utils.SendSuccessResponse(c, user)
}

