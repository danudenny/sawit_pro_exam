package repositories

import (
	"sawit_pro_exam/config"
	"sawit_pro_exam/domain/dto"
)

func UpdateUser(user *dto.GetProfileResponse) error {
	db := config.DB

	query := "UPDATE users SET phone_number = $1, full_name = $2 WHERE id = $3"
	_, err := db.Exec(query, user.PhoneNumber, user.FullName, user.ID)
	if err != nil {
		return err
	}

	return nil
}

