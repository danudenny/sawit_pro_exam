package repositories

import (
	"database/sql"
	"fmt"
	"sawit_pro_exam/config"
	"sawit_pro_exam/domain/dto"
)

func GetUserByPhoneNumber(phoneNumber string) (*dto.UserDto, error) {
	db := config.DB

	query := "SELECT id, full_name, phone_number, password FROM users WHERE phone_number = $1"
	row := db.QueryRow(query, phoneNumber)

	user := &dto.UserDto{}
	err := row.Scan(&user.ID, &user.FullName, &user.PhoneNumber, &user.Password)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("user not found")
		}
		return nil, err
	}

	return user, nil
}
