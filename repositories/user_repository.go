package repositories

import (
	"database/sql"
	"fmt"
	"sawit_pro_exam/config"
	"sawit_pro_exam/domain/dto"
)

func GetUserByID(userID int64) (*dto.GetProfileResponse, error) {
	db := config.DB

	query := "SELECT id, phone_number, full_name FROM users WHERE id = $1"
	row := db.QueryRow(query, userID)

	user := &dto.GetProfileResponse{}
	err := row.Scan(&user.ID, &user.PhoneNumber, &user.FullName)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("User not found")
		}
		return nil, err
	}

	return user, nil
}
