package repositories

import (
	"golang.org/x/crypto/bcrypt"
	"sawit_pro_exam/config"
	"sawit_pro_exam/models"
)

func CreateUser(user *models.User) error {
	query := "INSERT INTO users (phone_number, full_name, password) VALUES ($1, $2, $3)"
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		return err
	}

	_, err = config.DB.Exec(query, user.PhoneNumber, user.FullName, hashedPassword)
	if err != nil {
		return err
	}

	return nil
}
