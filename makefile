init:
	go mod init sawit_pro_exam

test:
	go test -v ./...

run:
	go run cmd/main.go

docker-up:
	docker-compose up

docker-down:
	docker-compose down
