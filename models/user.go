package models

type User struct {
	PhoneNumber string	`json:"phone_number"`
	FullName    string	`json:"full_name"`
	Password    string	`json:"password"`
}
