CREATE TABLE users (
   id SERIAL PRIMARY KEY,
   phone_number VARCHAR(15) NOT NULL,
   full_name VARCHAR(60) NOT NULL,
   password VARCHAR(64) NOT NULL
);